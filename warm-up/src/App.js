import React, { useState, useEffect } from "react";
import { Switch, Route, useHistory } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import useLocalStorage from "./useLocalStorage";
import UserContext from "./context/UserContext";
import { Home } from "./pages/Home/Home";
import Layout from "./pages/Layout/Layout";
import ModalLogin from "./components/ModalLogin/ModalLogin";
import PostDetail from "./components/PostDetail/PostDetail";
import NewPost from "./components/NewPost/NewPost";
import EditPost from "./components/EditPost/EditPost";

function App() {
  const [user, setUser] = useLocalStorage("user", []);
  useEffect(() => {}, [user]);

  return (
    <UserContext.Provider value={user}>
      <div className="container d-sm-flex justify-content-center p-2  w-100"
      style={{ 
        // minHeight: "100vh",
        backgroundColor: "#130f40",
      }}
      >
        <Layout setUser={setUser} user={user}>
          {user ? (
            <>
              <Switch>
                <Route exact path="/edit/:id" render={()=><EditPost/>}/>
                <Route exact path="/create_post" render={() => <NewPost />} />
                <Route exact path="/post/:id" render={() => <PostDetail />} />
                <Route exact path="/" render={() => <Home user={user} />} />
              </Switch>
            </>
          ) : (
            <Route
              exact
              path="/"
              render={() => <ModalLogin setUser={setUser} />}
            />
          )}
        </Layout>
      </div>
    </UserContext.Provider>
  );
}

export default App;
