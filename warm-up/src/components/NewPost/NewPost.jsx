import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import Axios from "axios";
import swal from "sweetalert";
import { Formik } from "formik";

const NewPost = () => {
  const history = useHistory();

  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");

  const sendPost = async (values) => {
    const { title, body } = values;
    try {
      swal({
        title: "Posting...",
        text: `title : ${title}`,
        icon: "info",
        buttons: true,
        confirmMode: "confirm",
      }).then((willConfirm) => {
        if (willConfirm) {
          Axios.post("https://jsonplaceholder.typicode.com/posts", {
            title,
            body,
          })
            .then(() => {
              swal({
                title: "Post Success",
                icon: "success",
                button: false,
                timer: 1500,
              });
              history.push("/");
            })
            .catch((err) => {
              swal({
                title: "Post Failed",
                text: `title : ${title}`,
                icon: "error",
                button: false,
                timer: 1500,
              });
            });
        } else {
          setBody("");
          setTitle("");
        }
      });
    } catch (error) {
      swal({
        title: "Post Failed",
        icon: "error",
        button: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  useEffect(() => {
    if (body && title) {
      sendPost({ title, body });
    }
  }, [body, title]);

  return (
    <div
      style={{
        minHeight: "100vh",
        padding: "0 10px",
      }}
    >
      <div className="mt-3">

      <Link 
      to="/">Back to Home</Link>
      </div>

      <h3
      className="mt-5"
      >New Post</h3>
      <div className="d-flex flex-column">
      <Formik
        initialValues={{ title, body }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          setTitle(values.title);
          setBody(values.body);
          setSubmitting(false);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form
          className="d-flex flex-column mt-3"
           onSubmit={handleSubmit}>
            <label
            className="mt-3"
             htmlFor="title">Title</label>
            <input
              type="text"
              name="title"
              id="title"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.title}
              placeholder="Enter title"
              required
            />
            <label 
            className="mt-5"
            htmlFor="body">Post</label>
            <textarea
              name="body"
              id="body"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.body}
              placeholder="Enter body"
              required
            />
            <button
            className="mt-3"
            style={{
              borderRadius: "20px",
            }}
             type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </form>
        )}
      </Formik>
      </div>

    </div>
  );
};

export default NewPost;
