import React, { useState, useEffect } from "react";
import Axios from "axios";
import swal from "sweetalert";
import { Link, useHistory } from "react-router-dom";

const PostDetail = () => {
  const history = useHistory();
  const postId = history.location.pathname.split("/")[2];
  const [post, setPost] = useState({});

  const getPostDetail = async () => {
    try {
      const res = await Axios.get(
        `https://jsonplaceholder.typicode.com/posts/${postId}`
      );
      setPost(res.data);
    } catch (error) {
      swal("Error", "Something went wrong", "error");
      console.log(error);
    }
  };

  const deletePost = async () => {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this post!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        Axios.delete(`https://jsonplaceholder.typicode.com/posts/${postId}`)
          .then((res) => {
            swal("Poof! Your post has been deleted!", {
              icon: "success",
            });
            history.push("/");
          })
          .catch((err) => {
            swal("Error", "Something went wrong", "error");
          });
      } else {
        swal("Your post is safe!");
      }
    });
  };

  const editPost = () => {
    history.push(`/edit/${postId}`);
  };

  useEffect(() => {
    getPostDetail();
  }, []);

  return (
    <div
      className="mt-5"
      style={{
        minHeight: "100vh",
        padding: "0 20px",
      }}
    >
      <Link to="/">Back to Home</Link>
      <div className="mt-3">
        <div className="mt-5">
          <span
            style={{
              textDecoration: "underline",
              fontSize: "1rem",
            }}
          >
            Title:
          </span>
          <p
            className={{
              fontFamily: "Circular-Loom",
              fontSize: "16px",
            }}
          >
            {post.title}
          </p>
        </div>
        <div className="mt-5">
          <span
            style={{
              textDecoration: "underline",
            }}
          >
            Post:
          </span>
          <p>{post.body}</p>
        </div>

        <div className="d-flex justify-content-center mt-5">
          <button
            style={{
              margin: "0 5px 0 0",
              borderRadius: "40px",
              width: "250px"
            }}
            onClick={deletePost}
          >
            remove post
          </button>
          <button
            style={{
              margin: "0 0 0 5px",
              borderRadius: "40px",
              width: "250px"
            }}
            className="ml-5"
            onClick={editPost}
          >
            edit post
          </button>
        </div>
      </div>
    </div>
  );
};

export default PostDetail;
