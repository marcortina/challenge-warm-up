import React from "react";
import { Link } from "react-router-dom";
import PostList from "../../components/PostList/PostList";

export const Home = ({ user }) => {
  return (
    <div className="mt-5">
      <div className="mb-5 d-flex justify-content-center">
        <Link
          to="/create_post"
          style={{
            padding: "15px 15px 2px 15px",
            borderBottom: "2px solid white",
            textDecoration: "none",
            cursor: "pointer",
            color: "white",
          }}
        >
          click here to post
        </Link>
      </div>
      <PostList />
    </div>
  );
};
