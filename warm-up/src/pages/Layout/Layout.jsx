import React from "react";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";

const Layout = ({ children, setUser }) => {
  return (
    <div>
      <Navbar setUser={setUser} />
      {children}
      <Footer />
    </div>
  );
};

export default Layout;
