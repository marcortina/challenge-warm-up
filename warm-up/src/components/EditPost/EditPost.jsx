import React, { useState, useEffect } from "react";
// import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import Axios from "axios";
import swal from "sweetalert";
import { Formik } from "formik";

const EditPost = () => {
  // const history = useHistory();
  const id = window.location.pathname.split("/")[2];

  const [post, setPost] = useState({
    title: "",
    body: "",
  });

  const getPostToEdit = async () => {
    try {
      const res = await Axios.get(
        `https://jsonplaceholder.typicode.com/posts/${id}`
      );
      setPost(res.data);
    } catch (err) {
      console.log(err);
      swal("Error", err.message, "error");
    }
  };

  const handleSubmitt = async (values) => {
    const { title, body } = values;

    try {
      await Axios.put(
        `https://jsonplaceholder.typicode.com/posts/${id}`,
        values
      );
      swal("Success", "Post updated successfully", "success");
    } catch (err) {
      console.log(err);
      swal("Error", err.message, "error");
    }
  };

  useEffect(() => {
    getPostToEdit();
  }, []);

  return (
    <div
      className="mt-5"
      style={{
        minHeight: "100vh",
      }}
    >
      <Link to="/">Back to Home</Link>
      <Formik
        initialValues={{
          title: post?.title,
          body: post?.body,
        }}
        touched={{
          title: true,
          body: false,
        }}
        onSubmit={(values, { setSubmitting }) => {
          values.title = values.title.trim();
          values.body = values.body.trim();
          setSubmitting(false);
          if (values.title !== "" && values.body !== "") {
            setTimeout(() => {
              swal({
                title: "Are you sure?",
                text: "Once updated, you will not be able to recover this imaginary file!",
                icon: "info",
                buttons: true,
                confirmMode: true,
              }).then((willConfirm) => {
                if (willConfirm) {
                  handleSubmitt(values);
                  setSubmitting(false);
                }
              });
            }, 400);
          } else {
            swal("Error", "Title and Body are required", "error");
          }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form 
          className="d-flex flex-column"
          style={{
            margin: "0 auto",
          }}
          onSubmit={handleSubmit}>
            <label
             htmlFor="title"
             className="mt-5"
             >Title</label>
            <input
              name="title"
              type="text"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.title}
              placeholder={post?.title}
              touched={touched.title}
              
            />
            <label 
            htmlFor="body"
            className="mt-5"
            >Body</label>
            <textarea
              name="body"
              type="text"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.body}
              placeholder={post?.body}
            />
            <div className="d-flex justify-content-center">
            <button
            style={{
              marginTop: "20px",
              borderRadius: "40px",
              width: "250px"
            }}
             type="submit" disabled={isSubmitting}>
              Submit
            </button>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default EditPost;
