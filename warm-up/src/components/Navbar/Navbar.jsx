import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import UserContext from "../../context/UserContext";
import swal from "sweetalert";

const Navbar = ({ setUser }) => {
  const history = useHistory();
  const user = useContext(UserContext);

  const logout = () => {
    swal({
      title: "Are you sure?",
      text: "You will be logged out",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        setUser(null);
        localStorage.removeItem("token");
        swal("You are logged out", {
          icon: "success",
        })
        history.push("/");
      } else {
        swal("You are safe!");
      }
    });
  };

  return (
    <div className="d-flex align-items-center pt-3">
      <span 
      className="w-100 text-end"
      style={{ fontWeight: "400", textTransform: "uppercase" }}
      >welcome </span>
      
      <span 
      className="w-100"
      style={{  fontWeight: "400",textTransform:"capitalize" }}
      >&nbsp;{user?.name}</span>
      {user ? (
        <div className="w-100 h-10 d-flex justify-content-end">
          <button
            style={{
              borderRadius: "25px",
              margin: "0px 10px",
              width: "150px",
            }}
            type="button"
            onClick={logout}
          >
            logout
          </button>
        </div>
      ) : null}
    </div>
  );
};

export default Navbar;
