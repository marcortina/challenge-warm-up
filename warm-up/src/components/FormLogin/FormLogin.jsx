import React from "react";
import Axios from "axios";
import { Formik } from "formik";
import swal from "sweetalert";

const FormLogin = ({ setUser }) => {
  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = "Required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }
    if (!values.password) {
      errors.password = "Required";
    }

    return errors;
  };

  const onSubmit = async (values) => {
    const { name, email, password } = values;

    try {
      const response = await Axios.post(
        `http://challenge-react.alkemy.org/?email=${values.email}&password=${values.password}`
      );
      setUser({ name, email, password, token: response.data.token });
      // localStorage.setItem("user", JSON.stringify(user));
      swal("Success", "Login Success", "success", {
        button: false,
        timer: 1500,
      });
    } catch (error) {
      swal("Error", "Login Failed", "error");
    }
  };
  // useEffect(() => {}, [user, setUser]);
  return (
    <div>
      <Formik
        initialValues={{ email: "", password: "", name: "" }}
        validate={validate}
        onSubmit={onSubmit}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="container" onSubmit={handleSubmit}>
            <input
              type="text"
              name="name"
              placeholder="name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.name}
              className="input-form w-100 mb-3"
              required
            />
            {errors.name && touched.name && errors.name}
            <input
              type="email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              className="input-form w-100 mb-3"
              required
            />
            {errors.email && touched.email && errors.email}
            <input
              type="password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              className="input-form w-100 mb-3  "
              required
            />
            {errors.password && touched.password && errors.password}
            <button
              type="submit"
              disabled={isSubmitting}
              className="button-form"
              style={{
                borderRadius: "25px",
              }}
            >
              Submit
            </button>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default FormLogin;
