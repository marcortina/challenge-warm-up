import React, { useState } from "react";

function useLocalStorage(key, initialValue) {
    const [storedValue, setStoredValue] = useState(() => {
        // localStorage.clear();
        console.log("useLocalStorage");
        try {
          const newItem = window.localStorage.getItem(key);
          return newItem ? JSON.parse(newItem) : initialValue;
        } catch (error) {
          console.log(error);
          return initialValue;
        }
      });
    
      const setValue = (value) => {
        try {
          setStoredValue(value);
          localStorage.setItem(key, JSON.stringify(value));
        } catch (error) {
          console.log(error);
        }
      };
    
      return [storedValue, setValue];
}

export default useLocalStorage;

