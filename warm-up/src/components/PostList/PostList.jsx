import React, { useEffect, useState } from "react";
import Axios from "axios";
import swal from "sweetalert";
import { Link } from "react-router-dom";

const PostList = () => {
  const [posts, setPosts] = useState([]);

  const isError = () => {
    swal({
      title: "Error!",
      text: "Something went wrong!",
      icon: "error",
      timer: 1500,
    });
  };

  const getPosts = async () => {
    try {
      const res = await Axios.get("https://jsonplaceholder.typicode.com/posts");
      setPosts(res.data);
    } catch (error) {
      isError();
    }
  };

  useEffect(() => {
    getPosts();
  }, [posts]);

  return (
    <div>
      <div className="d-inline-flex flex-wrap align-items-center">
        <h4>list of posts</h4>
        <span
          style={{
            fontSize: "10px",
            color:"#ff7979"
          }}
        >
          &nbsp;(click on the titles to see the details)
        </span>
      </div>

      <ul
        style={{
          border: "2px solid #fff",
          borderRadius: "20px",
          padding: "15px 5px 5px 5px",
          marginTop: "10px",
        }}
      >
        {posts?.map((post) => (
          <li
            key={post.id}
            style={{
              listStyle: "none",
              marginBottom: "10px",
              padding: "0 20px",
            }}
          >
            <Link to={`/post/${post.id}`} style={{ textDecoration: "none" }}>
              <p
                style={{
                  color: "#f6e58d",
                }}
              >
                "{post.title}"
              </p>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default PostList;
