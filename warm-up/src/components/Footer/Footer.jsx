import React from "react";

const Footer = () => {
  return (
    <div className="d-inline-flex justify-content-center align-items-center">
      <span
        style={{
          fontSize: "2rem",
          margin: "0 auto",
        }}
      >
        ®
      </span>
      <span>MAR-CORTINA</span>
    </div>
  );
};

export default Footer;
