import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../context/UserContext";
import FormLogin from "../FormLogin/FormLogin";

const ModalLogin = ({ setUser }) => {
  const user = useContext(UserContext);
  const [show, setShow] = useState(true);

  useEffect(() => {
    if (user !== null && user.length !== 0) {
      setShow(false);
    } else {
      setShow(true);
    }
  }, [user]);

  return (
    <>
      {show ? (
        <div
          className="container d-sm-flex p-1 rounded w-100"
          style={{
            marginTop: "20%",
            minHeight: "100vh",
            // backgroundColor: "#130f40",
          }}
        >
          <div className="row justify-content-center">
            <div className="col-sm-6">
              <FormLogin setUser={setUser} />
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default ModalLogin;
